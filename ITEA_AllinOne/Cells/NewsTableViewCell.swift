//
//  NewsTableViewCell.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import WebKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var webViewImage: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
