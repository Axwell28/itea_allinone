//
//  ProfileViewController.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import RealmSwift

class ProfileViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var cityPostLabel: UILabel!
    @IBOutlet var dayOfBirthdayLabel: UILabel!
    @IBOutlet var telephoneLabel: UILabel!
    @IBOutlet var regionLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    
    var userData : Results<UserProfile>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        userData = try! Realm().objects(UserProfile.self)
        setFields()
    }
    
    func setFields() {
        if let data = userData {
            if let info = data.first {
                nameLabel.text = info.name
                surnameLabel.text = info.lastName
                ageLabel.text = "\(info.age)"
                cityPostLabel.text = info.postCode
                dayOfBirthdayLabel.text = info.dateOfBirthday
                telephoneLabel.text = info.telephone
                regionLabel.text = info.region
                countryLabel.text = info.country
            }
        }
    }
    
    @IBAction func onLogoutButtonTapped(_ sender: Any) {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        navigationController?.popToRootViewController(animated: true)
    }
    
}
