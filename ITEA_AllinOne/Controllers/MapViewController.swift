//
//  MapViewController.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RealmSwift

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet var mapView: GMSMapView!
    
    var userData : Results<UserProfile>?
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
        userData = try! Realm().objects(UserProfile.self)
        setCurrentLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    func setCurrentLocation() {
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let marker = GMSMarker()
        if let data = userData?.first {
            if let location = data.location {
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(location.latitude), longitude: CLLocationDegrees(location.longtitude), zoom: 11.0)
            self.mapView.animate(to: camera)
            marker.position.latitude = location.latitude
            marker.position.longitude = location.longtitude
            }
        }
        marker.map = mapView
        self.locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let alert = UIAlertController(title: "Choice", message: "Go to news?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            self.goToNewsController()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true)
        return true
    }
    
    func goToNewsController() {
        let vc = UIStoryboard(name: "Functional", bundle: nil).instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onLogoutButtonTapped(_ sender: Any) {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onMyProfileButtonTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}
