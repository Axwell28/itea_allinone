//
//  LoginViewController.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import RealmSwift

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var emailText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var loginImage: UIImageView!
    
    var token : Results<TokenModel>?
    var email = String()
    var pass = String()
    var userData : UserProfile?
    var logged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        loginButton.layer.cornerRadius = 25
        loginImage.clipsToBounds = true
        loginImage.layer.cornerRadius = 20
        self.hideKeyboardWhenTappedAround()
        emailText.delegate = self
        passwordText.delegate = self
        token = try! Realm().objects(TokenModel.self)
        if let tok = token, tok.count > 0, tok[0].token.count > 0 {
            let uData = try! Realm().objects(UserProfile.self)
            print(uData.count)
            goToMapController()
        }
    }

    @IBAction func onLoginButtonTapped(_ sender: Any) {
        if !checkTextFields() {
            debugPrint("NOOOO its not work!")
        } else {
            if Reachability.isConnectedToNetwork() {
                let userInfo = UserProfile()
                let realm = try! Realm()
                let ref = Database.database().reference(withPath: "list")
                ref.observe(.value) { (snapshot) in
                    if let value = snapshot.value as? [[String : Any?]] {
                        for items in value {
                            //if let service = items.values as? [String : Any?] {
                                if let em = items["email"] as? String {
                                    self.email = em
                                    userInfo.email = em
                                }
                                if let pas = items["password"] as? String {
                                    self.pass = pas
                                    userInfo.password = pas
                                }
                                if let age = items["age"] as? Int {
                                    userInfo.age = age
                                }
                                if let city = items["city"] as? String {
                                    userInfo.city = city
                                }
                                if let country = items["country"] as? String {
                                    userInfo.country = country
                                }
                                if let dateOfBirthday = items["dateOfBirthday"] as? String {
                                    userInfo.dateOfBirthday = dateOfBirthday
                                }
                                if let lastName = items["lastName"] as? String {
                                    userInfo.lastName = lastName
                                }
                                if let postCode = items["postCode"] as? String {
                                    userInfo.postCode = postCode
                                }
                                if let region = items["region"] as? String {
                                    userInfo.region = region
                                }
                                if let telephone = items["telephone"] as? String {
                                    userInfo.telephone = telephone
                                }
                                if let name = items["name"] as? String {
                                    userInfo.name = name
                                }
                                if let location = items["location"] as? [String : Any?] {
                                    let locationData = Location()
                                    if let lat = location["latitude"] as? Double {
                                        locationData.latitude = lat
                                    }
                                    if let long = location["longitude"] as? Double {
                                        locationData.longtitude = long
                                    }
                                    userInfo.location = locationData
                                }
                            
                            if let enterEmail = self.emailText.text {
                                if let enterPass = self.passwordText.text {
                                    if userInfo.email == enterEmail , userInfo.password == enterPass {
                                        self.logged = true
                                        realm.beginWrite()
                                        let token = TokenModel()
                                        token.token = UUID().description
                                        realm.create(TokenModel.self, value: token)
                                        if let data = self.userData {
                                            realm.create(UserProfile.self, value: data)
                                        }
                                        try! realm.commitWrite()
                                        let ur = try! Realm().objects(UserProfile.self)
                                        debugPrint(ur.count)
                                        self.goToMapController()
                                        break
                                    }
                                }
                            }
                        }
                    }
                        if !self.logged {
                            self.showAlert(title: "Error", message: "No such user or incorrect password!")
                        }
                        DispatchQueue.main.async {
                            self.userData = userInfo
                            realm.beginWrite()
                            if let data = self.userData {
                                realm.create(UserProfile.self, value: data)
                            }
                                try! realm.commitWrite()
                        }
                }
            } else {
                self.showAlert(title: "No internet connection", message: "Please connect to internet to run application normaly")
            }
        }
    }
    
    func checkTextFields() -> Bool {
        if !emailText.text!.isEmpty {
            if !passwordText.text!.isEmpty {
                return true
            } else {
                showAlert(title: "Error", message: "Password textfielad is empty")
                return false
            }
        } else {
            showAlert(title: "Error", message: "Email textfielad is empty")
            return false
        }
    }
    
    func showAlert(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
    func goToMapController() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        navigationController?.pushViewController(vc, animated: false)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
