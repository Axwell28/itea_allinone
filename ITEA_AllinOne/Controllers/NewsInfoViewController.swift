//
//  NewsInfoViewController.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import WebKit

class NewsInfoViewController: UIViewController {

    
    @IBOutlet var imageWeb: WKWebView!
    @IBOutlet var tittleLabel: UILabel!
    @IBOutlet var contentText: UITextView!
    @IBOutlet var sourceLAbel: UILabel!
    
    var info = ResponseNewsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        tittleLabel.text = info.title
        contentText.text = info.content
        sourceLAbel.text = info.sourceName
        if let url = URL(string: info.urlToImage) {
            imageWeb.load(URLRequest(url: url))
        }
    }
}
