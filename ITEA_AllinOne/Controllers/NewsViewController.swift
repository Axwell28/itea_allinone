//
//  NewsViewController.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 7/22/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import ObjectMapper

class NewsViewController: UIViewController {

    @IBOutlet var newsTable: UITableView!
    
    var page = 1
    var maxPage = 1
    var fromSourceList : Results<ResponseNewsModel>?
    var dataList = [ResponseNewsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        newsTable.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        getNews(page: page)
        newsTable.delegate = self
        newsTable.dataSource = self
    }
   
    func getNews(page : Int) {
        if Reachability.isConnectedToNetwork() {
            
            var newInputList = [ResponseNewsModel]()
            
            var params : [String:Any] = [:]
            params["q"] = "Money"
            params["apiKey"] = "374d54f870974a5d82cdf792f59a847c"
            params["from"] = "2019-07-25"
            params["to"] = "2019-07-22"
            params["pageSize"] = 10
            params["page"] = page
            
            Alamofire.request("https://newsapi.org/v2/everything?pageSize=10&page=\(page)", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                let news = Mapper<NewsModel>().map(JSONObject: response.result.value)
                if let totalResults = news?.totalResults {
                    self.maxPage = totalResults / 10
                } else {
                    return
                }
                if let articles = news?.articles {
                    let news = ResponseNewsModel()
                    for items in articles {
                        news.content = items.content ?? ""
                        news.newsDescription = items.newsDescription ?? ""
                        news.sourceName = items.sourceName ?? ""
                        news.title = items.title ?? ""
                        news.urlToImage = items.urlToImage ?? ""
                        newInputList.append(news)
                    }
                }
                        let realm = try! Realm().objects(ResponseNewsModel.self)
                        if realm.count != 0 {
                            try! Realm().write {
                                 try! Realm().delete(realm)
                            }
                        }
                        self.dataList += newInputList
                        for items in self.dataList {
                            try! Realm().write {
                                try! Realm().create(ResponseNewsModel.self, value:  items)
                            }
                        }
                        DispatchQueue.main.async {
                            self.newsTable.reloadData()
                        }
                    }
        } else {
            self.dataList = try! Realm().objects(ResponseNewsModel.self).sorted(by: { $0.title.count > $1.title.count })
        }
    }
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell") as! NewsTableViewCell
        //Выносить в апдейт
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 25
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.blue.cgColor
        cell.titleLabel.text = dataList[indexPath.row].title
        cell.descriptionLabel.text = dataList[indexPath.row].newsDescription
        if let url = URL(string: dataList[indexPath.row].urlToImage) {
            cell.webViewImage.load(URLRequest(url: url))
        }
        //
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NewsInfoViewController") as! NewsInfoViewController
        vc.info = dataList[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if page < maxPage {
            if indexPath.row == 9 * page {
                self.page += 1
                getNews(page: page)
            }
        }
        
    }
    
}
