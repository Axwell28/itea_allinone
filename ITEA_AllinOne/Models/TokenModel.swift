//
//  TokenModel.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 8/5/19.
//  Copyright © 2019 G9. All rights reserved.
//

import Foundation
import RealmSwift

class TokenModel: Object {
    
    @objc dynamic var token : String = ""
    
}
