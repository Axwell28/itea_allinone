//
//  UserProfile.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 8/4/19.
//  Copyright © 2019 G9. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class UserProfile: Object {
    
     @objc dynamic var email = String()
     @objc dynamic var password = String()
     @objc dynamic var name = String()
     @objc dynamic var lastName = String()
     @objc dynamic var dateOfBirthday = String()
     @objc dynamic var age = 0
     @objc dynamic var city = String()
     @objc dynamic var telephone = String()
     @objc dynamic var country = String()
     @objc dynamic var region = String()
     @objc dynamic var postCode = String()
     @objc dynamic var location : Location?
    
}
class Location: Object {
    
    @objc dynamic var longtitude = Double()
    @objc dynamic var latitude = Double()
    
}
