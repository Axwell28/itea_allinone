//
//  ResponseNewsModel.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 8/5/19.
//  Copyright © 2019 G9. All rights reserved.
//

import Foundation
import RealmSwift

class ResponseNewsModel: Object {
    
    @objc dynamic var sourceName = ""
    @objc dynamic var title = ""
    @objc dynamic var newsDescription = ""
    @objc dynamic var urlToImage = ""
    @objc dynamic var content = ""
    
}
