//
//  NewsModel.swift
//  ITEA_AllinOne
//
//  Created by Alex Marfutin on 8/8/19.
//  Copyright © 2019 G9. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsModelArticle : Mappable {
    
    var sourceName : String?
    var title : String?
    var newsDescription : String?
    var urlToImage : String?
    var content : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        sourceName          <- map["source"]
        title               <- map["title"]
        newsDescription     <- map["description"]
        urlToImage          <- map["url"]
        content             <- map["content"]
    }
}


class NewsModel : Mappable {
    
    var status : String?
    var totalResults : Int?
    var articles : [NewsModelArticle]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status              <-  map["status"]
        totalResults        <-  map["totalResults"]
        articles            <-  map["articles"]
        
    }
    
}
